#!/usr/bin/python3

import shelve
import os
from urllib import parse
import random
import string
import webapp

PAGE_NOT_FOUND = """
<html>
    <body>
        <h1>Recurso no disponible: {resource}</h1>
    </body>
</html>
"""

PAGE_NOT_ALLOWED = """
<html>
    <body>
        <h1>Método no permitido: {method}</h1>
    </body>
</html>
"""

MAIN_PAGE = """
<html>
    <body>
        <h1>Bienvenido a Randomshort por Alex Lacoste</h1>
        <form action="/" method="POST">
            <div>
                <label> Url para recortar: </label>
                <input type="text" name="url" required>
            </div>
            <div>
                <input type="submit" value="SHORT">
            </div>
        </form>
        <h2> Url recortada: </h2>
        <pre>{current}</pre>
        <h3> Enlaces cortados: </h3>
        <pre>{dict}</pre>
    </body>
</html>
"""

class Shortener(webapp.webApp):

    def read_dict(self):
        if os.path.isfile('dict.db'):
            with shelve.open('dict.db', 'r') as db:
                self.dict = db['dict']
        else:
            with shelve.open('dict.db', 'c') as db:
                db['dict'] = self.dict
        db.close()

    def parse(self, request):
        # Quedarme con el nombre del recurso, el método y el cuerpo
        data = {}
        body_start = request.find('\r\n\r\n')
        if body_start == -1:
            # No hay cuerpo en la peticion
            data['body'] = None
        else:
            # Hay cuerpo en la petición
            data['body'] = request[body_start + 4:]
        parts = request.split(' ')
        data['resource'] = parts[1]
        data['method'] = parts[0]
        return data

    def process(self, data):
        # Procesa los datos de la petición. Devuelve el código htpp de la respuesta y una pag HTML
        if data['method'] == 'GET':
            http_code, html_page = self.get(data['resource'], "")
        elif data['method'] == 'POST':
            http_code, html_page = self.post(data['body'])
        else:
            http_code = "405 Method Not Allowed"
            html_page = PAGE_NOT_ALLOWED.format(method=data['method'])
        return http_code, html_page

    def get(self, resource, current):
        if resource == '/':
            http_code = "200 OK"
            dict = "\n".join([f'Recortada: http://localhost:{self.port}{key} '
                              f'--- Real: {value}' for key, value in self.dict.items()])
            html_page = MAIN_PAGE.format(dict=dict, current=current)
        elif resource in self.dict.keys():
            http_code = f"307 Temporary Redirect\r\nLocation: {self.dict[resource]}"
            html_page = '<html></html>'
        else:
            http_code = "404 Not Found"
            html_page = PAGE_NOT_FOUND.format(resource=resource)
        return http_code, html_page

    def get_key(self, value):
        # Devuelve la llave del valor en el diccionario
        for key, val in self.dict.items():
            if val == value:
                return key

    def post(self, body):
        #parseo con parse_qs los campos del formulario
        fields = parse.parse_qs(body)
        # este try es por si alguien intenta enviar el campo url vacío con la extensión RESTclient
        try:
            url = fields['url'][0]
        except KeyError:
            http_code = "400 Bad Request"
            html_page = '<html>No se ha introducido ninguna url</html>'
            return http_code, html_page

        # Si no empieza con http:// o https:// añado https por defecto
        if not url.startswith('http://') and not url.startswith('https://'):
            url = 'https://' + url

        # Si la url no está en el diccionario, la añado
        if url not in self.dict.values():
            new_rsc = self.gen_resource()
            self.dict[new_rsc] = url
            with shelve.open('dict.db', 'w') as db:
                db['dict'] = self.dict
            db.close()
        # En current guardo la url que se acaba de recortar
        current = f"\nRecortada: http://localhost:{self.port}{self.get_key(url)} --- Real: {url}\n"
        #.. Muestro el estado actual de la pag web
        http_code, html_page = self.get('/', current)
        return http_code, html_page

    # Genero un recurso aleatorio (mezcla de 6 caracteres ascii y digitos)
    def gen_resource(self):
        new_rsc = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(6))
        new_rsc = '/' + new_rsc
        while new_rsc in self.dict.keys(): # Pruebo hasta que se encuentre un recurso que no esté en el diccionario
            new_rsc = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(6))
            new_rsc = '/' + new_rsc
        return new_rsc

if __name__ == '__main__':
    app = Shortener(1234, '127.0.0.1')