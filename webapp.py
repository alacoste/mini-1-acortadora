import socket

class webApp:
    def __init__(self, port, hostname):
        self.port = port
        self.hostname = hostname
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.dict = {}

        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((self.hostname, self.port))
        self.socket.listen(10)  # .. 10 conexiones simultáneas máximo TCP
        self.read_dict()
        try:
            while True:
                print("Esperando conexión... Puerto: ", self.port)
                clientsocket, addr = self.socket.accept()
                print("Conexión establecida con: ", addr)
                request = clientsocket.recv(2048)
                print(request)
                request = request.decode('utf-8')
                # Manejar la petición
                parsed_req = self.parse(request)

                # Proceso la petición y genero la respuesta
                code, html = self.process(parsed_req)

                # Enviar respuesta al cliente
                respuesta = "HTTP/1.1 " + code + "\r\n\r\n" + html + "\r\n"
                clientsocket.send(respuesta.encode('utf-8'))
                clientsocket.close()
        except KeyboardInterrupt:
            print("closing server...")
            self.socket.close()

    def parse(self, request):
        print("Definicion de parse")
        return None

    def process(self, parsed_request):
        print("Definicion de process")
        return "", ""

    def read_dict(self):
        print("Definicion de read_dict")

